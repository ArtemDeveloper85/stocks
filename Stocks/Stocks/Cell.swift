//
//  Cell.swift
//  Stocks
//
//  Created by Artem Novikov on 20.06.21.
//

import UIKit
import Alamofire
import Kingfisher

class Cell: UITableViewCell {
    @IBOutlet weak var nameCompanyLabel: UILabel!
    @IBOutlet weak var symbolCompanyLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var view: UIView!
    
    private var logoDataRequest: DataRequest?
    private var logoDownLoadTask: DownloadTask?
    let urlComponentForLogo = "/logo?token="

    override func awakeFromNib() {
        super.awakeFromNib()
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.blue.cgColor
    }
    
    func update(with company: ListElement) {
        cancelLogoLoading()
        self.symbolCompanyLabel.text = company.symbol
        self.nameCompanyLabel.text = company.name
        let urlForLogo = "\(Constant.baseUrl)\(company.symbol)\(urlComponentForLogo)\(Constant.token)"
        ServerManager.shared.fetchGenericObject(url: urlForLogo, for: company.symbol) { (result: Result<Logo, NetworkError>) in
            switch result {
                case .success(let logo):
                    if let url = logo.url,
                       let urlForLogo = URL(string: url) {
                        self.logoDownLoadTask = self.logoImageView.kf.setImage(with: urlForLogo)
                    } else {
                        self.logoImageView.image = .defaultLogo
                        
                    }
                case .failure(_):
                    self.logoImageView.image = .defaultLogo
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cancelLogoLoading()
        logoImageView.image = nil
        symbolCompanyLabel.text = nil
        nameCompanyLabel.text = nil
    }
    
    private func cancelLogoLoading() {
        logoDataRequest?.cancel()
        logoDataRequest = nil
        logoDownLoadTask?.cancel()
        logoDownLoadTask = nil
    }
}
