//
//  Alert.swift
//  Stocks
//
//  Created by Artem Novikov on 30.06.21.
//

import UIKit

class Alert {
    
    static func showAlert(_ error: NetworkError, viewController: UIViewController) {
        
        let alert = UIAlertController(title: "Error", message: "\(error)", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .destructive, handler: nil)
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
    }
    
}
