// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let list = try? newJSONDecoder().decode(List.self, from: jsonData)

import Foundation

// MARK: - ListElement
struct ListElement: Codable {
    let symbol: String
    let exchange: Exchange
    let exchangeSuffix: ExchangeSuffix
    let exchangeName: ExchangeName
    let name, date: String
    let type: TypeEnum
    let iexID: String?
    let region: Region
    let currency: Currency
    let isEnabled: Bool
    let figi, cik, lei: String?

    enum CodingKeys: String, CodingKey {
        case symbol, exchange, exchangeSuffix, exchangeName, name, date, type
        case iexID = "iexId"
        case region, currency, isEnabled, figi, cik, lei
    }
}

enum Currency: String, Codable {
    case cad = "CAD"
    case eur = "EUR"
    case ils = "ILS"
    case usd = "USD"
}

enum Exchange: String, Codable {
    case nas = "NAS"
    case nys = "NYS"
    case por = "POR"
    case usamex = "USAMEX"
    case usbats = "USBATS"
    case uspac = "USPAC"
}

enum ExchangeName: String, Codable {
    case cboeBzxUSEquitiesExchange = "Cboe Bzx U S Equities Exchange"
    case exchangeNameCBOEBZXUSEQUITIESEXCHANGE = "CBOE BZX U.S. EQUITIES EXCHANGE"
    case exchangeNameNASDAQALLMARKETS = "NASDAQ - ALL MARKETS"
    case exchangeNameNEWYORKSTOCKEXCHANGEINC = "NEW YORK STOCK EXCHANGE, INC."
    case nasdaqAllMarkets = "Nasdaq All Markets"
    case nasdaqCapitalMarket = "NASDAQ CAPITAL MARKET"
    case nasdaqNgsGlobalSelectMarket = "NASDAQ/NGS (GLOBAL SELECT MARKET)"
    case nasdaqNmsGlobalMarket = "NASDAQ/NMS (GLOBAL MARKET)"
    case newYorkStockExchangeInc = "New York Stock Exchange Inc"
    case nyseArca = "Nyse Arca"
    case nyseMktLlc = "Nyse Mkt Llc"
    case portal = "Portal"
}

enum ExchangeSuffix: String, Codable {
    case empty = ""
    case ua = "UA"
    case uf = "UF"
    case un = "UN"
    case up = "UP"
}

enum Region: String, Codable {
    case us = "US"
}

enum TypeEnum: String, Codable {
    case ad = "ad"
    case cs = "cs"
    case et = "et"
    case ps = "ps"
    case rt = "rt"
    case typeStruct = "struct"
    case ut = "ut"
    case wi = "wi"
    case wt = "wt"
}

typealias List = [ListElement]
