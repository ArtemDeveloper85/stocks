//
//  Constant.swift
//  Stocks
//
//  Created by User on 7.06.21.
//

import Foundation

struct Constant {
    static let token = "pk_2c55d0a9178948ccb9f00e23a0e43914"
    static let baseUrl = "https://cloud.iexapis.com/stable/stock/"
    static let baseUrlList = "https://cloud.iexapis.com/beta/ref-data/symbols?token"
}

struct Logo: Codable {
    var url: String?
}

enum NetworkError: Error {
    case badUrl
    case custom(Error)
}
