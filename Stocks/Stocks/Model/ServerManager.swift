//
//  RequestQuote.swift
//  Stocks
//
//  Created by User on 6.06.21.
//

import Foundation
import Alamofire

class ServerManager {
    static let shared = ServerManager()
    
    func fetchGenericObject<T: Decodable>(url: String, for symbol: String, completionHandler: @escaping (Result<T, NetworkError>) -> Void) {
        guard let url = URL(string: url) else {
            completionHandler(.failure(.badUrl))
            return
        }
        
        AF.request(url).responseDecodable(of: T.self) { response in
            let result = response.result
            switch result {
            case .success(let generObject):
                completionHandler(.success(generObject))
            case .failure(let error):
                completionHandler(.failure(.custom(error)))
            }
        }
    }

    func downloadListCompany(completionHahdler: @escaping (Result<[ListElement], NetworkError>) -> Void) {
        guard let url = URL(string: "\(Constant.baseUrlList)=\(Constant.token)")  else {
            completionHahdler(.failure(.badUrl))
            return
        }
        AF.request(url).responseDecodable(of: [ListElement].self) { response in
            let result = response.result
            switch result {
                case .success(let list):
                    completionHahdler(.success(list))
                case .failure(let error):
                    completionHahdler(.failure(.custom(error)))
            }
        }
    }
}
