//
//  Localization.swift
//  Stocks
//
//  Created by Artem Novikov on 8.07.21.
//

import Foundation

enum L10n {
    static func localizated(_ key: String) -> String {
        NSLocalizedString(key, comment: "")
    }
    
    enum main {
        enum search {
            static var title: String {
                localizated("main.search.title")
            }
        }
    }
    
    enum alert {
        enum  error {
            static var title: String {
                localizated("alert.error.title")
            }
            enum button {
                static var ok: String {
                    localizated("alert.error.button.ok")
                }
            }
        }
    }
}
