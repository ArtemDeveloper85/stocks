//
//  Stock1.swift
//  Stocks
//
//  Created by Artem Novikov on 21.06.21.
//

import Foundation

struct Stock: Codable {
    var symbol, companyName, primaryExchange, calculationPrice: String?
    var openSource: String?
    var closeSource: String?
    var latestPrice: Double?
    var latestSource, latestTime: String?
    var latestUpdate: Int?
    var previousClose: Double?
    var previousVolume: Int?
    var change, changePercent: Double?
    var iexMarketPercent: Double?
    
}
