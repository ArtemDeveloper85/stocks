//
//  ViewController.swift
//  Stocks
//
//  Created by User on 4.06.21.
//

import UIKit
import Alamofire
import Kingfisher

class ViewController: UIViewController {
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var companySymbolLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceChangeLabel: UILabel!
    @IBOutlet weak var logoCompanyImageView: UIImageView!
    @IBOutlet weak var changePercentLabel: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var indicatorActivity: UIActivityIndicatorView!
    
    var symbol: String?
    let urlComponentForStock = "/quote?token="
    let urlComponentForLogo = "/logo?token="
    private var list: [ListElement] = []
    private var filtredList: [ListElement] = []
    var isSearching = false
    private var logoDataRequest: DataRequest?
    private var logoDownLoadTask: DownloadTask?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.placeholder = L10n.main.search.title
        indicatorActivity.startAnimating()
        searchBar.showsCancelButton = true
        tableview.dataSource = self
        tableview.delegate = self
        searchBar.delegate = self
        let xib = UINib(nibName: .cell, bundle: nil)
        tableview.register(xib, forCellReuseIdentifier: .cell)
        ServerManager.shared.downloadListCompany { [weak self] result in
            guard let self = self else { return }
            switch result {
                case .success(let list):
                    self.list = list
                    self.tableview.reloadData()
                    self.indicatorActivity.stopAnimating()
                    self.indicatorActivity.isHidden = true
                case .failure(let error):
                    self.showAlert(error: error)
            }
        }
    }
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        companyNameLabel.text = .dash
        companySymbolLabel.text = .dash
        priceLabel.text = .dash
        priceChangeLabel.text = .dash
        priceChangeLabel.textColor = .white
        changePercentLabel.text = .dash
        changePercentLabel.textColor = .white
        let symbol = getList()[indexPath.row].symbol
        self.symbol = symbol
        let urlForStock = "\(Constant.baseUrl)\(self.symbol ?? "")\(urlComponentForStock)\(Constant.token)"
        ServerManager.shared.fetchGenericObject(url: urlForStock,
                                                for: symbol) { [weak self] (result: Result<Stock, NetworkError>) in
            guard let self = self else {
                return
            }
            switch result {
                case .success(let stock):
                    self.companyNameLabel.text = stock.companyName
                    self.companySymbolLabel.text = stock.symbol
                    self.priceLabel.text = stock.price
                    self.changePercentLabel.text = stock.formattedChangePercent
                    self.changePercentLabel.textColor =  stock.priceColor
                    self.priceChangeLabel.text = stock.changePrice
                    self.priceChangeLabel.textColor = stock.priceColor
                case .failure(let error):
                    self.showAlert(error: error)
            }
        }
        
        let urlForLogo = "\(Constant.baseUrl)\(self.symbol ?? "")\(urlComponentForLogo)\(Constant.token)"
        ServerManager.shared.fetchGenericObject(url: urlForLogo,
                                                for: symbol) { [weak self] (result: Result<Logo, NetworkError>) in
            guard let self = self  else { return }
            self.cancelLogoLoading()
            switch result {
                case .success(let logo):
                    if let url = logo.url,
                       let urlForLogo = URL(string: url) {
                        self.logoDownLoadTask = self.logoCompanyImageView.kf.setImage(with: urlForLogo)
                    } else {
                        self.logoCompanyImageView.image = .defaultLogo
                    }
                case .failure(let error):
                    self.showAlert(error: error)
                    self.logoCompanyImageView.image = .defaultLogo
            }
        }
    }
}



extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getList().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableview.dequeueReusableCell(withIdentifier: .cell) as? Cell else {
            fatalError("Cell not registered.")
        }
        let company = getList()[indexPath.row]
        cell.update(with: company)
        indicatorActivity.stopAnimating()
        return cell
    }
    
}

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filtredList = list.filter { element in
            element.name.lowercased().prefix(searchText.count) == searchText.lowercased()
        }
        isSearching = true
        tableview.reloadData()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        searchBar.text = .emptyString
        tableview.reloadData()
    }
}

extension UIViewController {
    
    func showAlert(error: NetworkError,
                   title: String = L10n.alert.error.title,
                   actionTitle: String = L10n.alert.error.button.ok) {
        let alert = UIAlertController(title: title, message: "\(error)", preferredStyle: .alert)
        let action = UIAlertAction(title: actionTitle, style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}

extension ViewController {
    func getList() -> [ListElement] {
        isSearching ? filtredList : list
    }
}

extension String {
    static let dash = "-"
    static let emptyString = ""
    static let cell = "Cell"
    static let percent = "%"
    static let dollar = "$"
}

extension Stock {
    var formattedChangePercent: String? {
        if let changePercent = self.changePercent {
            return (String(format: "%.2f", changePercent)) + .percent
        }
        
        return nil
    }
    
    var price: String {
        .dollar + "\(latestPrice ?? 0)"
    }

    var changePrice: String {
        "\(change ?? 0)"
    }
    
    var priceColor: UIColor? {
        guard let changePrice = change else { return nil }
        switch changePrice {
            case  changePrice where changePrice < 0:
                return .red
            case  changePrice where changePrice > 0:
                return .green
            case changePrice where changePrice == 0:
                return .white
            default:
                return nil
        }
    }
}

extension ViewController {
    func cancelLogoLoading() {
        logoDataRequest?.cancel()
        logoDataRequest = nil
        logoDownLoadTask?.cancel()
        logoDownLoadTask = nil
    }
}
