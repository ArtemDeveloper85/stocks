//
//  UIImage+Utils.swift
//  Stocks
//
//  Created by Artem Novikov on 16.07.21.
//

import UIKit

extension UIImage {
    static var defaultLogo: UIImage? { UIImage(named: "No image")}
}
